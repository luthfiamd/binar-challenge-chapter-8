import React from 'react';

function Update() {
    return(
        <div className="container mt-5 mb-5 justify-center">
            <h1>Update Player Baru:</h1>
                <form>
                    <label>Username: </label>
                    <input type="text" name="username" required/>
                    <br/><br/>
                    <label>Password: </label>
                    <input type="password" name="password"/>
                    <br/><br/>
                    <label>Email: </label>
                    <input type="text" name="email"/>
                    <br/><br/>
                    <label>Experience: </label>
                    <input type="text" name="experience"/>
                    <br/><br/>
                    <label>Level: </label>
                    <input type="text" name="level"/>
                    <br/><br/>
                    <button type="submit" className="btn btn-primary" name="Update">Submit</button>
                </form>
        </div>
    );
}

export default Update;