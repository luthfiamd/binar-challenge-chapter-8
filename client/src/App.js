import './App.css';
import { BrowserRouter as Router,Route,Link } from 'react-router-dom';
import Create from './components/Create';
import Update from './components/Update';
import Search from './components/Search';

function App() {
  return (
    <Router>
          <div className="container-fluid">
            <nav className="navbar navbar-expand-lg navbar-light bg-primary">
                <button className="navbar-toggler toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
 
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                      <li className="nav-item">
                          <Link className="nav-link text-black" to="/create">Create</Link>
                      </li>
                      <li className="nav-item">
                          <Link className="nav-link" to="/update">Update</Link>
                      </li>
                    </ul>
                    <div className="ml-5 mr-2">
                      <Route component={Search}/>
                    </div>
                </div>
            </nav>             
          </div>
          <div>
            <Route path="/create"  component={Create}/>
            <Route path="/update" component={Update}/>
          </div>
        </Router>
  );
}

export default App;